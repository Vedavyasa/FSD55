let addbutton = document.getElementById('addbtn');
let errorMsg = document.getElementById('errormsg');
let tasklist = document.getElementById('tasklist');
let Tasks = []
let todoappp = () => {
  let Usertask = document.getElementById('usergiventask');

  if (Usertask.value.trim() == "") {
    errorMsg.innerHTML = "Please Enter  Valid Task";
    errorMsg.style.color = "red";
    errorMsg.style.fontSize = '10px';

  } else {

    errorMsg.innerHTML = "";
    Tasks.push(Usertask.value);
    Usertask.value = "";
    console.log(Tasks);
    tasklist.innerHTML = ""
    displayTasks();


  }

}

function displayTasks() {
  Tasks.forEach((task,index) => {
    let lst = document.createElement('li');
    lst.innerHTML = task;
    lst.classList.add('lstele');
    lst.addEventListener('click', () => {
      lst.classList.toggle('taskcompleted')
    })
    let icon = document.createElement('i');
    icon.classList.add('fa-solid', 'fa-delete-left')
    icon.onclick = ()=>{
      console.log(Tasks);
      // alert(index)
      Tasks.splice(index,1);
      tasklist.innerHTML="";
      displayTasks();

    }

    let divcontainer = document.createElement('div');
    divcontainer.classList.add('todotask')

    divcontainer.appendChild(lst);
    divcontainer.appendChild(icon)
    tasklist.appendChild(divcontainer);
  })

}
addbutton.addEventListener('click', todoappp);